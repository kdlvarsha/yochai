<?php
	// enables wigitized sidebars
	if ( function_exists('register_sidebar') )

	// Sidebar Widget
	// Location: the sidebar
	register_sidebar(array('name'=>'Chikong in Shfayim',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	));
	// Header Widget
	// Location: right after the navigation
	register_sidebar(array('name'=>'Header',
		'before_widget' => '<div class="widget-area widget-header"><ul>',
		'after_widget' => '</ul></div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	// Footer Widget
	// Location: at the top of the footer, above the copyright
	register_sidebar(array('name'=>'Footer',
		'before_widget' => '<div class="widget-area widget-footer"><ul>',
		'after_widget' => '</ul></div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	
	
	register_sidebar(array('name'=>'Home Page Circle Link',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	
	register_sidebar(array('name'=>'Contact Sidebar',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	));
	register_sidebar(array('name'=>'Contact Middle',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	));
	register_sidebar(array('name'=>'Shfayim header',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	));

	// post thumbnail support
	add_theme_support( 'post-thumbnails' );
	// adds the post thumbnail to the RSS feed
	function cwc_rss_post_thumbnail($content) {
	    global $post;
	    if(has_post_thumbnail($post->ID)) {
	        $content = '<p>' . get_the_post_thumbnail($post->ID) .
	        '</p>' . get_the_content();
	    }
	    return $content;
	}
	add_filter('the_excerpt_rss', 'cwc_rss_post_thumbnail');
	add_filter('the_content_feed', 'cwc_rss_post_thumbnail');

	// custom menu support
	add_theme_support( 'menus' );
	if ( function_exists( 'register_nav_menus' ) ) {
	  	register_nav_menus(
	  		array(
	  		  'header-menu' => 'Header Menu',
	  		  'footer-menu' => 'Footer Menu'
	  		)
	  	);
	}

	// removes detailed login error information for security
	add_filter('login_errors',create_function('$a', "return null;"));
	
	// removes the WordPress version from your header for security
	function wb_remove_version() {
		return '<!--built by CQuinnDesign-->';
	}
	add_filter('the_generator', 'wb_remove_version');
	
	// Removes Trackbacks from the comment cout
	add_filter('get_comments_number', 'comment_count', 0);
	function comment_count( $count ) {
		if ( ! is_admin() ) {
			global $id;
			$comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
			return count($comments_by_type['comment']);
		} else {
			return $count;
		}
	}
	// invite rss subscribers to comment
	function rss_comment_footer($content) {
		if (is_feed()) {
			if (comments_open()) {
				$content .= 'Comments are open! <a href="'.get_permalink().'">Add yours!</a>';
			}
		}
		return $content;
	}
	
	// custom excerpt ellipses for 2.9+
	function custom_excerpt_more($more) {
		return 'Read More &raquo;';
	}
	add_filter('excerpt_more', 'custom_excerpt_more');
	// no more jumping for read more link
	function no_more_jumping($post) {
		return '<a href="'.get_permalink($post->ID).'" class="read-more">'.'&nbsp; Continue Reading &raquo;'.'</a>';
	}
	add_filter('excerpt_more', 'no_more_jumping');
	
	// category id in body and post class
	function category_id_class($classes) {
		global $post;
		foreach((get_the_category($post->ID)) as $category)
			$classes [] = 'cat-' . $category->cat_ID . '-id';
			return $classes;
	}
	add_filter('post_class', 'category_id_class');
	add_filter('body_class', 'category_id_class');

	// adds a class to the post if there is a thumbnail
	function has_thumb_class($classes) {
		global $post;
		if( has_post_thumbnail($post->ID) ) { $classes[] = 'has_thumb'; }
			return $classes;
	}
	add_filter('post_class', 'has_thumb_class');

	// ***********************************slider******************************************************
	add_action( 'init', 'codex_custom_slider_init' );

function codex_custom_slider_init() {

  $labels = array(

    'name' => _x('Slider', 'post type general name'),

    'singular_name' => _x('Slider', 'post type singular name'),

    'add_new' => _x('Add New', 'Slider'),

    'add_new_item' => __('Add New Slider'),

    'edit_item' => __('Edit Slider'),

    'new_item' => __('New Slider'),

    'all_items' => __('All Slider'),

    'view_item' => __('View Slider'),

    'search_items' => __('Search Slider'),

    'not_found' =>  __('No Slider found'),

    'not_found_in_trash' => __('No Slider found in Trash'),

    'parent_item_colon' => '',

    'menu_name' => 'Sliders'

  );



  $args = array(

    'labels' => $labels,

    'public' => true,

    'publicly_queryable' => true,

    'show_ui' => true,

    'show_in_menu' => true,

    'query_var' => true,

    'rewrite' => true,
	
	'exclude_from_search' => true, #################(for search )#############

    'capability_type' => 'post',

    'has_archive' => true,

    'hierarchical' => false,

    'menu_position' => 51,

    'supports' => array( 'title','thumbnail','editor')

  );

  register_post_type('slider',$args);

}



############################## CUSTOM MENU FOR Slider POSTS ###############################################

add_filter( 'manage_edit-slider_columns', 'my_edit_slider_columns' ) ;

function my_edit_slider_columns( $columns ) {



    $columns = array(

        'cb' => '<input type="checkbox" />',

        'title' => __( 'Slider Title' ),

        'featured-image' => __( 'Slider Image' ),

        'date' => __( 'Date' )

    );

    return $columns;

}

add_action( 'manage_slider_posts_custom_column', 'my_manage_slider_columns', 20, 2 );



function my_manage_slider_columns( $column, $post_id ) {

    global $post;

    switch( $column ) {



        case 'featured-image' :

            $varimage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');

            echo  '<img src="'. $varimage[0].'" alt="" height="60" width="60"  />';

            break;

           

        default :

            break;

    }

}



add_action( 'admin_head', 'gallery_icons1' );

function gallery_icons1() {

    ?>

    <style type="text/css" media="screen">

        #menu-posts-slider .wp-menu-image {

            background: url(<?php bloginfo('template_url') ?>/images/image--plus.png) no-repeat 6px -17px !important;

        }

	#menu-posts-imagegallery:hover .wp-menu-image, #menu-posts-photogallery.wp-has-current-submenu .wp-menu-image {

            background-position:6px 7px!important;

        }

    </style>

<?php }
?>
