<?php
/**
 * Template Name: Shiatsu Page
 

 *
 * @package TheGem
 */
get_header(); ?>

<body class="light text-2" id="full-size">
<!-- Pre loader -->
<div id="loader" class="loader"></div>
<div id="app" >

<!--Sidebar End-->

    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                           placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
    <div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
            <!--Top Menu Start -->
        <ul class="nav navbar-nav">
                <!-- Notifications -->
            <li id="icon-show">
                <a class="nav-link ml-2" data-toggle="control-sidebar"><span> סייקי שיאצו </span><i class="fa fa-bars" aria-hidden="true"></i></a>
            </li>
            <!-- User Account-->
        </ul>
    </div>
<div class="container-fluid mb-height blur">
        <div class="row mb-height" >
                <div class="col-md-9 marpad0 respon-1">
                    
                    <div class="page height-full" id="background-slider">
                        <!-- Slideshow container -->
                        <div class="slideshow-container">
                          <!-- Full-width images with number and caption text -->
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/quite.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>

                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/depth.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>
                          
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/connection.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>
                          
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/postal_code.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>
                          
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/compatible.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>

                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/release.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>
                          
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/confirmed.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>
                          
                          <div class="mySlides fad">
                            <img src="<?php echo get_bloginfo('template_directory'); ?>/img/calm.jpg" style="width:100%">
                            <div class="text">שקט</div>
                          </div>

                        <!--   Next and previous buttons -->
                           <div style="text-align:center" class="circle-bottom">
                          <span class="dot" onclick="currentSlide(1)"></span> 
                          <span class="dot" onclick="currentSlide(2)"></span> 
                          <span class="dot" onclick="currentSlide(3)"></span>
                          <span class="dot" onclick="currentSlide(4)"></span> 
                          <span class="dot" onclick="currentSlide(5)"></span> 
                          <span class="dot" onclick="currentSlide(6)"></span>             
                        </div>
                        </div>
                        <!-- The dots/circles -->       
                    </div>

                </div>
                
            <div class="col-md-3 col-xs-12 padd-top-30 " style="background: #160925;" >
                <aside class="rightfix">
                <div class="media_txthight"></div>
                        <div class=" textbg mobile-scroll" style="background: #160925; max-height: 84vh; overflow-y: scroll; padding:0 6px;">
                             <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
                            <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40" style="height:20px"></div>
                             <div class="btn btnstyle hidden-xs"><a href="<?php echo home_url(); ?>/צור קשר "> להזמנת טיפול </a></div>  
                        </div>
                </aside>
            </div>
            
            
            
            
            
            <div class="col-xs-12 marpad0 respon-2 carsual-text-callbtn">       
                <div class="page" >
                                                                    
                    <div id="myCarousel" class="carousel " data-ride="carousel" >
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                            <li data-target="#myCarousel" data-slide-to="5"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            
                            <div class="item active">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/quite.jpg" alt="Chania">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a><i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </div>
                            </div>
                            </div>

                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/depth.jpg" alt="Chicago" >
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </div>
                              </div>
                            </div>

                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/connection.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </div>
                              </div>
                            </div>
                            
                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/postal_code.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
                              </div>
                            </div>
                            
                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/compatible.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i></div>
                              </div>
                            </div>
                            
                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/release.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </div>
                              </div>
                            </div>
                            
                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/confirmed.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i></div>
                              </div>
                            </div>
                            
                            <div class="item">
                              <img src="<?php echo get_bloginfo('template_directory'); ?>/img/calm.jpg" alt="New York">
                              <div class="text-3">
                              <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול</a> <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
                              </div>
                            </div>
                        </div>


<!-- Left and right controls --
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>-->

                    </div>                  
                </div>
            </div>
            
            
            
            
            
            
        </div><!--row-->
</div>

<!-- Right Sidebar -->
<aside class="control-sidebar fixed menubgcolor ">
    <div class="slimScroll">
        <div class="sidebar-header">
            <h5 class="pull-right webtext"> סייקי שיאצו </h5>
            <a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
        </div>
        <div class="p-3 pull-right menustyle">
           
           <?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>     	
		 <div class="btn btnstyle-1"><a href="<?php echo home_url(); ?>"> להזמנת טיפול </a></div>
           
            
        </div>
    </div>
</aside>
<!-- /.right-sidebar -->






<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
<div class="control-sidebar-bg shadow menubgcolor fixed"> </div>



</div>
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<!--auto slide show-->
<script>
var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 2000); // Change image every 2 seconds
}
</script>


<!--
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>-->
<script>

       $(document).ready(function(){
      $("#icon-show").click(function(){
      $("#icon-show").hide();
      });
      $(".paper-nav-toggle").click(function(){
      $("#icon-show").show();  
      });
      });
</script>

<script>

       $(document).ready(function(){
      $("#icon-show").click(function(){
      $(".text-3").hide();
      });
      $(".paper-nav-toggle").click(function(){
      $(".text-3").show();  
      });
      });
</script>

<script>
      
       $(document).ready(function(){ 
       
      $("#icon-show").click(function(){
      if (window.matchMedia('(max-width: 767px)').matches) {
        //...
      $(".blur").css("filter","blur(2px)");   
    }
      });
      $(".paper-nav-toggle").click(function(){
      $(".blur").css("filter","blur(0)"); 
      });    
      });
      
      $(".nano").nanoScroller({
    alwaysVisible: true,
});
</script>
<!--swip Query in mobile on touch-->
    <script>
    (function ($) {
    var touchStartX = null;

    $('.carousel').each(function () {
        var $carousel = $(this);
        $(this).on('touchstart', function (event) {
            var e = event.originalEvent;
            if (e.touches.length == 1) {
                var touch = e.touches[0];
                touchStartX = touch.pageX;
            }
        }).on('touchmove', function (event) {
            var e = event.originalEvent;
            if (touchStartX != null) {
                var touchCurrentX = e.changedTouches[0].pageX;
                if ((touchCurrentX - touchStartX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('prev');
                } else if ((touchStartX - touchCurrentX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('next');
                }
            }
        }).on('touchend', function () {
            touchStartX = null;
        });
    });

})(jQuery);
    </script>



</body>