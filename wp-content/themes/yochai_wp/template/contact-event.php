<?php
/**
 * Template Name: Contact Event 
 *
 * @package TheGem
 */
get_header(); ?>

<body class="light contact-us-menu contact-event" id="full-size" class="">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> צור קשר   </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> צור קשר   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
				<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
                	 
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid contact-yochai respon-1">
			<div class="row">
			    <!--left-contact-->
				<div class="col-sm-4 left-contact contact-scroll">
					<div class="">
					    <div  class="col-sm-12 font-right-b"></div>
						
															
						<div class="col-sm-12 font-right-e" >שיעורים / סדנאות </div>
						
														
						<div class="col-sm-12 font-center-b" >בקרוב יפורסמו התאריכים </div>
						
						
						
						
						
					</div>
				</div>
				
				<!--middle-contact-->
				<div class="col-sm-5 center-contact text-right">
				    <div style="height:40%;  background-color: #E5BC00;">
						 <div  class="col-sm-12 font-right-b p-t-20" style="font-size:20px; font-weight:normal;">שירותים</div>
						<div class="col-md-12 p-b-20"><a href="<?php echo home_url(); ?>/שירותים"><span class="font-right-b">טיפולים </span><span class="font-left-n">  - הילינג | סייקי שיאצו</span></a></div>
						
					</div>	
				
					<div style="height:60%;" class="background-home">
						<div class="col-sm-12" style="position: absolute; bottom: 5px;">
							<div class="font-right-b"><a href="#" style="color:#fff;">     יוחאי דור סייקי שיאצו, הילינג וצ'יקונג  <i class="fa fa-facebook-square" style="padding-left:4px; font-size:19px;"></i></a></div> 
						</div>
					</div>
				</div>
				
				<!--right-contact-->
				<div class="col-sm-3 right-contact" style="padding-right: 0; border-bottom: ;">
					<div style="height:70px; width:100%"></div>
					<div class="mobile-scroll con-scro-600">
						<div class="contact-section">
							<div class="col-sm-12 p-b-20 bold">יוחאי דור</div>
							<div class="col-sm-12 p-b-10 ">052-3524381<span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone_icon.png"/></span>  </div>
							<div class="col-sm-12 p-b-10 "> קיבוץ שפיים  <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/></span>  </div>
							<div class="col-sm-12 p-b-20 "> רחוב הברזל 47 תל אביב <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/></span> </div>
						</div>
						
						<div class="contact-form">
							<div class="col-sm-12  p-b-10" style="color: #fff; opacity:.7;">  </div>
							<form>
								<div class="col-sm-12 form-group " > 
									<input type="text" class="form-control" name="" placeholder="שם ">
								
								</div>
								
								 <div class="col-sm-12 form-group " > 
									<input type="text" class="form-control" name="" placeholder="טלפון">
									
								</div>
								<div class="col-sm-12 form-group " > 
									<input type="text" class="form-control" name="" placeholder=" מייל">
									
								</div>
								<div class="col-sm-12 form-group " > 
									<textarea class="form-control" placeholder=" הודעה " rows="4"></textarea>
								</div>
								<div class="col-sm-12 form-group text-left"> 
									<button type="submit" class="btn">  שלח/י  </button>
								</div>
							</form>
						</div>
				</div></div>
			</div>
		</div>
		
		
		<!--mobile view-->
		
		
		<div class="moble-contact respon-2" style="background-color:#160925;" >
			<div style="text-align:center" class="portion-1">
				<div class="col-xs-12" style="background-color:#E5BC00; border-radius:50px; padding:1px;">
					<div class="col-xs-4" style="padding-left:0;">
					  <span class="dot" onclick="currentSlide(1)">  שירותים  </span> 
					</div>
					<div class="col-xs-4">
					  <span class="dot" onclick="currentSlide(2)">  לפרטים  </span>
					</div>
					<div class="col-xs-4" style="padding-right:0;">
					  <span class="dot" onclick="currentSlide(3)">  שלח/י הודעה  </span> 
					</div>
				</div>
			</div>

			<div class="slideshow-container portion-2">
				 
					<div class="mySlides fde">
							<div style="position:relative; height:70%;">									 
									  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/contact_image_mobile.png" style="width:100%">
									  <div class="text bold text-3"><a href="tel:0523524381 " target="_blank">  התקשר/י עכשיו <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
							</div>
							<div style="position:relative; height:30%;">
								<div class="first-contact" style="text-align:right;">
									<div  class="col-sm-12 font-right-b p-t-20"><a href="#">שירותים</a></div>
									<div class="col-md-12 p-b-20"><a href="#"><span class="font-right-b">טיפולים </span><span class="font-left-n">  - הילינג | סייקי שיאצו</span></a></div>
								<!--	<div class="col-md-12 p-b-40"><a href="#"><span class="font-right-b">שיעורים  </span><span class="font-left-n" >  </span></a></div>-->
								</div>
							</div>
					</div>

					<div class="mySlides fde">
							<div style="position:relative; height:70%;">									 
									  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/contact_image_mobile.png" style="width:100%">
									  <div class="text bold text-3"><a href="tel:0523524381 " target="_blank">  התקשר/י עכשיו <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
							</div>
							<div style="position:relative; height:30%;">
								<div class="contact-section">
									<div class="col-sm-12 p-b-20 bold" style="font-weight:bold;">יוחאי דור</div>
									<div class="col-sm-12 p-b-10 ">052-3524381 <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone_icon.png"/> </div>
									<div class="col-sm-12 p-b-10 "> קיבוץ שפיים  <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/></span>  </div>
									<div class="col-sm-12 p-b-20 ">  רחוב הברזל 47 תל אביב <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/></span>  </div>
								</div>
							</div>					
					</div>

					<div class="mySlides fde">
					<div class="main-form">
					    <div class="contact-form">
							
							<form>
								<div class="col-sm-12 form-group"> 
									<input type="text" class="form-control" name="" placeholder=" שם ">
								</div>
								 <div class="col-sm-12 form-group"> 
									<input type="text" class="form-control" name="" placeholder=" טלפון">
								</div>
								<div class="col-sm-12 form-group"> 
									<input type="text" class="form-control" name="" placeholder="  מייל">
								</div>
								<div class="col-sm-12 form-group"> 
									<textarea class="form-control" placeholder=" הודעה " rows="4"></textarea>
								</div>
								<div class="col-sm-12 form-group text-center"> 
									<button type="submit" class="btn">  שלח/י  </button>
								</div>
							</form>
						</div>
					</div>
					</div>
			
					<!--<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
						<a class="next" onclick="plusSlides(1)">&#10095;</a>
				  -->
				
			</div>
		</div><!-- mobile-view-->
			</div>	
		
</div>

<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script>

      
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>
<script>

       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>
</body>
