<?php
/**
 * Template Name: HomePage
 *
 * @package TheGem
 */

get_header(); ?>


<body class="index_1" id="full-size" style="color:black;">
<!-- Pre loader -->

    <video autoplay muted loop id="myVideo">
      <source src="<?php echo get_bloginfo('template_directory'); ?>/img/desktop BG_1.mov" type="video/mp4">
    </video>
 
<div id="loader" class="loader"></div>
<div id="app">
        
<!--Sidebar End-->
<div class="">
    <div class="pos-f-t">
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
            <div class="search-bar">
                <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                       placeholder="start typing...">
            </div>
            <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
               aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
        </div>
    </div>
</div>
   <div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
            <!--Top Menu Start -->
        <ul class="nav navbar-nav">
                <!-- Notifications -->
            <li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> דף הבית </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
            <!-- User Account-->
        </ul>
    </div>

</div>
<div class="page height-full blur">
    <div class="bgimages">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="fullspace">
    
                <?php dynamic_sidebar('Home Page Circle Link'); ?> 
                    
                         
                        
                            <!--<div class="roundtext typewriter">-->
                            <!--       <h3>Source Energy Healing</h3> -->
                            <!--        <p> יוחאי דור </p>-->
                            <!--    </div>-->
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <!-- bottom-button-->
    <div class="btn-hidee"></div>
    <div class=" index-bottom">
    <div class="col-xs-12 bottom-button respon-3" >
        <div class="col-xs-4">
           <a href="<?php echo home_url(); ?>/הילינג"><i class="fa fa-circle fa-border"></i> <br> צ׳הילינג </a>
        </div>
        <div class="col-xs-4">
              <a href="<?php echo home_url(); ?>/שיאצו"> <i class="fa fa-circle fa-border"></i> <br> שיאצו </a>
        </div>
        <div class="col-xs-4">
             <a href="<?php echo home_url(); ?>/צ׳יקונג">  <i class="fa fa-circle fa-border"></i> <br>צ׳יקונג </a>
        </div>
    </div>
    </div>
    
    
    
    
</div>
<!-- Right Sidebar -->
<aside class="control-sidebar fixed menubgcolor " >
    <div class="slimScroll">
        <div class="sidebar-header">
            <h5 class="pull-right webtext"> דף הבית </h5>
            <a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
        </div>
        <div class="p-3 pull-right menustyle">
          
                <?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
                   
           
        </div>
    </div>
   
</aside>
<div class="control-sidebar-bg shadow menubgcolor fixed"></div>

</div>
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>

<script>
$(document).ready(function() {     
    $('.circlefirst a').hover(function(){  
       $('.circlefirst').addClass('circle-border'); 
        $('.first-circle').addClass('ico-circle');    
    $('.firstborder').addClass('border-horizontal');   
     
    },     
    function(){   
    $('.circlefirst').removeClass('circle-border'); 
        $('.first-circle').removeClass('ico-circle'); 
      $('.firstborder').removeClass('border-horizontal');   
    
    });
});   
</script>

<script>
$(document).ready(function() {     
    $('.circlesec a').hover(function(){  
       $('.circlesec').addClass('circle-border'); 
        $('.second-circle').addClass('ico-circle');    
    $('.firstborder-2').addClass('border-horizontal');   
       
    },     
    function(){   
    $('.circlesec').removeClass('circle-border'); 
        $('.second-circle').removeClass('ico-circle'); 
      $('.firstborder-2').removeClass('border-horizontal');   
    
    });
});   
</script>

<script>
$(document).ready(function() {     
    $('.circlethird a').hover(function(){  
       $('.circlethird').addClass('circle-border'); 
        $('.third-circle').addClass('ico-circle');    
    $('.firstborder-3').addClass('border-horizontal');   
       
    },     
    function(){   
    $('.circlethird').removeClass('circle-border'); 
        $('.third-circle').removeClass('ico-circle'); 
      $('.firstborder-3').removeClass('border-horizontal');   
    
    });
});   
</script>


<script>
       $(document).ready(function(){
    $("#icon-show").click(function(){
    $("#icon-show").hide();
    });
    $(".paper-nav-toggle").click(function(){
    $("#icon-show").show();  
    });
    });
</script>
<script>

       $(document).ready(function(){
    $("#icon-show").click(function(){
    $(".index-bottom").hide();
    });
    $(".paper-nav-toggle").click(function(){
    $(".index-bottom").show();  
    });
    });
</script>
<script>

       $(document).ready(function(){ 
       
    $("#icon-show").click(function(){
    if (window.matchMedia('(max-width: 767px)').matches) {
        //...
    $(".blur").css("filter","blur(2px)");   
    }
    });
    $(".paper-nav-toggle").click(function(){
    $(".blur").css("filter","blur(0)"); 
    });  
    });
</script>
<!--type writter effect-->
<script>

                      var auto_refresh = setInterval(
function () {
    $('.typewriter').load('textheading').fadeIn("slow");
}, 19800); // refresh every 10000 milliseconds
                   
</script>

</body>
<?php wp_footer(); ?>
