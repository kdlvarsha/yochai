<?php
/**
 * Template Name: Chi kong 
 *
 * @package TheGem
 */
get_header(); ?>

<body class="chi_cong cong-menu" id="full-size">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> צ׳יקונג   </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> צ׳יקונג   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
			<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		
		<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid contact-yochai respon-1">
			<div class="row">
			    <!--left-portion-->
				<div class="col-sm-5 center-img">
					<div style="height:45%;" class="img_pading-1"> <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture.png" style="height:100%; width:100%;"/>	</div>	
					<div style="height:55%;" class="img_pading-2"> <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-1.png" style="height:100%; width:100%;">	</div>
				</div>
				
				<!--middle-portion-->
				<div class="col-sm-4 center-img">
				    <div style="height:70%;" class="img_pading-3"> <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-2.png" style="height:100%; width:100%;"/>	</div>	
					<div style="height:30%;" class="img_pading-4"> <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-3.png" style="height:100%; width:100%;"/>	</div>
				</div>
				
				<!--right-portion-->
				<div class="col-sm-3 right-cong-text" style="padding-right: 0; height:100%">
					<div style="height:14%; width:100%"></div>
					<div class="mobile-scroll" style="height:75%; overflow-y:scroll; margin-right: 5px;">
						<div class="text-section">
							<div class="col-sm-12 p-b-10 "> <span>Zhineng Qigong </span> </div>
							<div class="col-sm-12 p-b-20 "> <span>  אומנות ריפוי עצמית עתיקה  </span> </div>
							<div class="col-sm-12 p-b-10 "> 
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
							</div>
						</div>						
					</div>
					<div style="height:10%;">						
								<div class="col-sm-12 text-left"> 
									<a href="<?php echo home_url(); ?>/צור קשר "><button type="submit" class="btn">  להזמנת טיפול  </button></a>
								</div>
							</form>
					</div>
				</div> <!--right-portion-->
			</div><!--row-->
		</div><!--respon-1-->
		
		
		<!--mobile view-->		
		<div class="moble-contact respon-2">
			<div class="col-sm-12 col-xs-12 right-cong-text" style="padding: 0; height:100%">
					<div style="height:70px; width:100%"></div>
					<div class="mobile-scroll" style="height:60vh; overflow-y:scroll; margin-right: 8px;    margin-bottom: 20px;">
						<div class="text-section">
							<div class="col-sm-12 p-b-10 "> <span>Zhineng Qigong </span> </div>
							<div class="col-sm-12 p-b-20 "> <span>  אומנות ריפוי עצמית עתיקה  </span> </div>
							<div class="col-sm-12 p-b-10 "> 
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
							</div>
						</div>						
					</div>
					<div style="height:30vh;" class="carsual-cong-callbtn">
						<div id="myCarousel" class="carousel " data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<li data-target="#myCarousel" data-slide-to="1"></li>
								<li data-target="#myCarousel" data-slide-to="2"></li>
								<li data-target="#myCarousel" data-slide-to="3"></li>
							  </ol>

							  <!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="item active">
								  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture.png" alt="Chania" style=" width:100%; min-height:250px; " />
								  <div class="text-3">
								  <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -4px; margin-top: -6px; margin-bottom: -5px;margin-left: 10px;"></i> </a></div>
								</div>
								</div>

								<div class="item">
								  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-1.png" alt="Chicago" style=" width:100%; min-height:250px;" />
								  <div class="text-3">
								  <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול  <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -4px; margin-top: -6px; margin-bottom: -5px;margin-left: 10px;"></i> </a></div>
								  </div>
								</div>

								<div class="item">
								  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture.png" alt="New York" style=" width:100%; min-height:250px;" />
								  <div class="text-3">
								  <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -4px; margin-top: -6px; margin-bottom: -5px;margin-left: 10px;"></i> </a></div>
								  </div>
								</div>
								
								<div class="item">
								  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-1.png" alt="New York" style=" width:100%; min-height:250px;" />
								  <div class="text-3">
								  <div class="text bold"><a href="tel:0523524381 " target="_blank">להזמנת טיפול <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -4px; margin-top: -6px; margin-bottom: -5px;margin-left: 10px;"></i> </a></div>
								  </div>
								</div>
								
							</div>
						</div>					
					</div>
			</div> <!--right-portion-->
		</div><!-- mobile-view-->
		</div><!--blur-->	
		
</div>

<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

<!--blur-->
<script>   
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>

<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<!--carsual-button-hide-show-->
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>
<!--swip Query in mobile on touch-->
	<script>
	(function ($) {
    var touchStartX = null;

    $('.carousel').each(function () {
        var $carousel = $(this);
        $(this).on('touchstart', function (event) {
            var e = event.originalEvent;
            if (e.touches.length == 1) {
                var touch = e.touches[0];
                touchStartX = touch.pageX;
            }
        }).on('touchmove', function (event) {
            var e = event.originalEvent;
            if (touchStartX != null) {
                var touchCurrentX = e.changedTouches[0].pageX;
                if ((touchCurrentX - touchStartX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('prev');
                } else if ((touchStartX - touchCurrentX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('next');
                }
            }
        }).on('touchend', function () {
            touchStartX = null;
        });
    });

})(jQuery);
	</script>


</body>