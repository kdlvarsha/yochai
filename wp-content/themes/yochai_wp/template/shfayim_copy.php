<?php
/**
 * Template Name: Chikong in Shfayim with scroller
 *
 * @package TheGem
 */

get_header(); ?>

<body class="single_event_details cong-menu" id="full-size">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> שיעור צ׳יקונג  </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> שיעור צ׳יקונג   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
					<ul>
						<li> <a href="<?php echo home_url(); ?>"> דף הבית </a></li>
                	<li> <a href="<?php echo home_url(); ?>/שיאצו/">שיאצו</a></li>
                	<li> <a href="<?php echo home_url(); ?>/הילינג""> הילינג </a></li>
                	<li> <a href="<?php echo home_url(); ?>/ צ׳יקונג "> צ׳יקונג </a></li>
                	<li> <a href="<?php echo home_url(); ?>/ אודות "> אודות </a></li>
                	<li> <a href="<?php echo home_url(); ?>/category/השראה/ "> השראה </a></li>
                	<li> <a href="<?php echo home_url(); ?>/צור קשר""> צור קשר </a></li>
                	
					</ul>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		
	<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid contact-yochai respon-1" style="background-color: #160925;" >
			<div class="row">
				<!--top-portion-banner-->
				<div class="col-sm-9 p-0">
							<!--banner-->
							<div class="col-sm-12 p-0 banner-details">
								<div class="box">
									<h2> צ׳יקונג בשפיים </h2>
									<p> יום שלישי | 3.10.2018 </p>
									<a href="<?php echo home_url(); ?>/ שיעור צ׳יקונג " class="btn"> להרשמה לסדנא  </a>
								</div>
							</div>
							<!--left-portion-->
							<div class="col-md-12" style="padding:0;">
							    <div class="mobile-scroll round" style="height:100%; overflow-y:scroll; margin-right:10px; max-height: 335px;">
							<div class="col-sm-6 center-img " padding-right: 7px;">
							
							<?php dynamic_sidebar('Chikong in Shfayim')?>
							
							</div>
							
							<!--middle-portion-->
							<div class="col-sm-6 center-img event-cong-text" style="padding-top: 36px;padding-bottom: 16px;">
								<div class="text-section">
								
										<div class="col-sm-12 p-b-20 p-t-20" style="padding-top: 0px !important;
    margin-top: -6px;"> <span> שיעור חוויתי ופתוח לכולם  </span> </div>
										<div class="col-sm-12 p-b-10 "> 
											 <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
										</div>
									</div>					
							</div>
							</div>
							</div><!--middle -portion-->
				</div><!--col-sm-9-->
				
				<!--right-portion-->
				<div class="col-sm-3 event-right-text" style="padding-right: 0; height:100%">
					<div style="height:20%; width:100%"></div>
					<div class="mobile-scroll" style="height:80%; overflow-y:scroll; ">
						<div class="text-section">
							<div class="col-sm-12 p-b-20 "> <span>  “בכל אדם קיים כוח ריפוי פנימי ! תפקדנו להכיר אותו ולאפשר לו להאיר החוצה מתוכנו”  </span> </div>
							
						</div>						
					</div>
				</div> <!--right-portion-->
			</div><!--row-->
		</div><!--respon-1-->
		
		
		<!--mobile view-->		
		<div class="respon-2 mobile-event clearfix">
			<div style="height:70px; width:100%; background-color:#E5BC00;"></div>
			<div class="mobile-event-banner">
				<div class="box">
					<h2> צ׳יקונג בשפיים </h2>
					<p> יום שלישי | 3.10.2018 </p>
					<a href="<?php echo home_url(); ?>/ שיעור צ׳יקונג " class="btn"> להרשמה לסדנא  </a>
				</div>
			</div>
			<div class="col-xs-12 mobile-banner-bottom ">
				<p>  “בכל אדם קיים כוח ריפוי פנימי ! תפקדנו להכיר אותו ולאפשר לו להאיר החוצה מתוכנו”  </p>
			</div>
			<div class="container-fluid text-section">
				<div class="col-xs-12 p-b-20 p-t-20"> <span> שיעור חוויתי ופתוח לכולם  </span> </div>
				<div class="col-xs-12 p-b-10 ">
					<p> -בצי' קונג, אנו לומדים להביא את תשומת הלב, המודעות <br>(mindfulnees)</p>
								<p>  אל תוך הגוף, לקבל ולהכיל ‏את התחושות הרגשות ‏‏והחוויות הנמצאות עכשיו, ברגע זה בתוך גופינו. לאפשר הנעה של התכווצות או הרגשה לא הרמונית במקום מסוים בגופנו, ולבחור להתחבר מחדש למקורות האנרגטיים האינסופיים, הנמצאים מסביבנו בטבע, ועמוק בתוך תוכנו.  </p>
								<p>  זהו תהליך מופלא של שינוי והתחדשות, של איחוד של יכולם הריפוי הטבעית , המאפשרת זרימה , חכמה, יצירה שלווה ושמחה בחיים שלנו. </p>
								<p> אני מזמין אתכם לחוויה של חיבור פנימה קלילות זרימה והנאה <br> בברכה יוחאי דור מטפל ומורה לצ'י קוג   </p>
								<p>   -בצי' קונג, אנו לומדים להביא את תשומת הלב, המודעות <br>(mindfulnees)</p>
								<p>  אל תוך הגוף, לקבל ולהכיל ‏את התחושות הרגשות ‏‏והחוויות הנמצאות עכשיו, ברגע זה בתוך גופינו. לאפשר הנעה של התכווצות או הרגשה לא הרמונית במקום מסוים בגופנו, ולבחור להתחבר מחדש למקורות האנרגטיים האינסופיים, הנמצאים מסביבנו בטבע, ועמוק בתוך תוכנו.    </p>
								<p>  זהו תהליך מופלא של שינוי והתחדשות, של איחוד של יכולם הריפוי הטבעית , המאפשרת זרימה , חכמה, יצירה שלווה ושמחה בחיים שלנו. </p>
								<p>  אני מזמין אתכם לחוויה של חיבור פנימה קלילות זרימה והנאה <br> בברכה יוחאי דור מטפל ומורה לצ'י קוג </p>
								<p>   -בצי' קונג, אנו לומדים להביא את תשומת הלב, המודעות <br>(mindfulnees)</p>
								<p>  אל תוך הגוף, לקבל ולהכיל ‏את התחושות הרגשות ‏‏והחוויות הנמצאות עכשיו, ברגע זה בתוך גופינו. </p>
								<p>  שלום, שמי יוחאי דור, לומד מלמד, מנחה ומטפל מתמחה בתחום שיאצו, צ'י-קונג, והילינג . </p>
								<p>  אה"אני מאמין", כי אנו נמצאים בזמנים המבקשים מודעות והתעוררות לכוח הריפוי הפנימי הנמצא בכל אדם. </p>
								<p>  אני מאמין באהבה ככוח העוצמתי ביותר לריפוי, כוח המאפשר שינוי מהותי לחיים בריי קיימא.   </p>
								<p>  תהליך הריפוי המתרחש תוך כדי טיפול, הינו חשיפה לטבענו המקורי, עידוד ופינוי מקום להזרמת אנרגיית החיים הנובעת מלב האדם פנימה.   </p>
								<p>  הטיפולים אינדיבידואלים ומותאמים באופן אישי, ההקשבה העמוקה לצרכים ולאדם הנמצא לפני, מאשרת לכל החסימות והדפוסים לצוף, לעלות ולהשתחרר. כך מתחזק הקשר עם העצמי האמיתי, והתוצאות מדהימות ומופלאות.   </p>
								<p>  שנים של התפתחות ועבודה עם רבים, והתגובות הנלהבות שאני מקבל ממטופלים ותלמידים, מוכיחות ומביאות אותי לידי הבנה בהירה, עד כמה חשובה הקריאה העכשווית לשימוש בכלים נגישים אלו, במתנות הטבע הברוכות. עד כמה משמעותית ומאפשרת שיפור ניכר באיכות החיים , בבריאות הפיזית הנפשית והרוחנית, ובמערכות יחסים עם עצמינו, האנשים סביבו ועם העולם הגדול שבחוץ.   </p>
								<p>  אני רוצה להודות, עם כל ליבי והערכתי, לכל המורים שליוו אותי לאורך כל השנים, על הלימוד, ההשראה , הידע החכמה והבהירות שעברה דרכם אלי.  </p>
								<p>  אני רוצה גם להודות למטופלים ולתלמידים שפגשתי וליוויתי לאורך השנים והם היו למורים הגדולים שלי!  </p>
								<p>   ותלמידים, מוכיחות ומביאות אותי לידי הבנה בהירה, עד כמה חשובה הקריאה העכשווית לשימוש בכלים נגישים אלו, במתנות הטבע הברוכות. עד כמה משמעותית ומאפשרת שיפור ניכר באיכות החיים , בבריאות הפיזית הנפשית והרוחנית, ובמערכות יחסים עם עצמינו, האנשים סביבו ועם  </p>
								<p>  העולם הגדול שבחוץ.   </p>
								<p>   אני רוצה להודות, עם כל ליבי והערכתי, לכל המורים שליוו אותי  <br>לאורך כל השנים, על הלימוד, ההשראה , הידע החכמה  <br>והבהירות שעברה דרכם אלי. <br> אני רוצה גם להודות למטופלים ולתלמידים שפגשתי וליוויתי לאורך  <br> השנים והם היו למורים הגדולים שלי! </p>
								<p>    </p>
				</div>
			</div>
			<div style="border-bottom:1px solid #fff; border-right:1px solid #fff;">
				<div class="col-xs-12" style="padding:0;">  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-5.png" style="height:100%; width:100%;"/> </div>
				<div class="col-xs-12" style="padding:0;">  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-6.png" style="height:100%; width:100%;"/>	</div>	
				<div class="col-xs-12" style="padding:0;">  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-7.png" style="height:100%; width:100%;"/>	</div>	
			</div>
		</div><!-- mobile-view-->
		
		
	</div><!--blur-->			
</div>

<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

<!--blur-->
<script>   
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>

<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<!--carsual-button-hide-show-->
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>



</body>