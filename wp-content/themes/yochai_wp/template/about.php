<?php
/**
 * Template Name: About
 *
 * @package TheGem
 */
get_header(); ?>

<body class="about-page cong-menu" id="full-size">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span class="mb-c"> אודות   </span><i class="fa fa-bars mb-c" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> אודות   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
				<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		
		<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid contact-yochai respon-1">
			<div class="row">
			    <!--left-portion-->
				<div class="col-sm-4 center-img">
					<div style="height:100%;"> <img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-8.png" style="height:100%; width:100%;"/>	</div>
				</div>
				
				<!--middle-portion-->
				<div class="col-sm-5 middle-about-text">
				    <div class="about-scroll" style="height:100%; overflow-y:scroll; ">
						<div class="text-section">
							<div class="col-sm-12 p-b-10 "> 
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
							</div>
						</div>						
					</div>
				</div>
				
				<!--right-portion-->
				<div class="col-sm-3 right-about-text" style="padding-right: 0; height:100%">
					<div style="height:14%; width:100%"></div>
					<div class="mobile-scroll" style="height:76%; overflow-y:scroll; margin-right:7px; ">
						<div class="text-section">
							<div class="col-sm-12 "> <h3>הכשרות ולימודים מקצועיים בארץ ובחו״ל </h3> </div>
							<div style="width:55px; height:5px; background-color:#E5BC00; margin: 8px 15px 20px; float:right;" > </div>
							<div class="col-sm-12 p-b-20 first-por">
								<span> 1998 </span>
								<p> "רייקי מאסטר" 1-2-3 </p>
								<span> 2000 </span>
								<p> QiGong- Taoist inner alcomy-<br>Cosmic healing<br><br> עם מאסטר "מאנטאק צ'ייה".</p>
								<span> 2000-2004 </span>
								<p> טאו-שיאצו עם "ריוקיו אנדו סנסיי" , במרכז הישראלי לטאו-שיאצו.  </p>
								<span> 2007-2012 </span>
								<p> "סייקי שיאצו" עם צביקה קליסר סנסיי , בבית ספר לסייקי שיאצו ישראל.  </p>
								<span> 2014-2018 </span>
								<p>  Wisdom Healling Qigong (Zhineng) with master "Mington Gu" </p>
								<span> 2015-2017 </span>
								<p>  Sheng Zhen Gong with master Li Junfeng </p>
								<span> 2016-2018 </span>
								<p>  Zhineng Qigong with teachers from china Harmonious Big Family </p>
							</div>
							<div class="col-sm-12 "> <h3>הסמכות </h3> </div>
							<div style="width:55px; height:5px; background-color:#E5BC00; margin: 8px 15px 20px; float:right;" > </div>
							<div class="col-sm-12 p-b-10 second-por">
								<p>  מטפל בארץ ובחול בהילינג דרך צ'י-קונג </p>
								<p>  מטפל ב"רייקי"  </p>
								<p>  מטפל בכיר בסייקי שיאצו- טאו שיאצו  </p>
								<p>   מדריך ואסיסטנט בסייקי שיאצו לילדים ובוגרים </p>
								<p>   מורה בשיטת - Wisdom Healling Qigong (Zhineng) </p>
								<p>   מורה בשיטת - Sheng Zhen Gong - לפתיחת הלב </p>
							</div>
							<div class="col-sm-12 "> <h3> ניסיון </h3> </div>
							<div style="width:55px; height:5px; background-color:#E5BC00; margin: 8px 15px 20px; float:right;" > </div>
							<div class="col-sm-12 p-b-20 third-por">
								<span> רייקי  </span>
								<p>  טיפולים והדרכת קבוצות   </p>
								<span>  שיאצו  </span>
								<p class="p-t-10">  הדרכת והעברת קבוצות שיאצו לילדים, בגנים ובבתי ספר, ובניית מערכי שיעור במסגרות אלו  </p>
								<p>   מתן טיפולים בשיאצו, פרטניים , בילדים במגוון גילאים וקשיים </p>
								<p>   "פרוייקט אקי"ם" - מתן טיפולים שבועיים באקי"ים למשך כ-3 שנים </p>
								<p>  מדריך ואסיסטנט בבית ספר לשיאצו (בית ספר לסייקי שיאצו ישראל), מעל 3 שנים.  </p>
								<p>  נסיון של מעל 18 שנה במתן טיפולי שיאצו בקליניקה פרטית ובקופת חולים מכבי לכל קשת הגילאים ובמגוון רחב של קשיים ותחלואות (גוף -נפש)  </p>
								<span> הילינג   </span>
								<p class="p-t-10">  נסיון של מעל 18 שנה בטיפולים פרטניים בהילינג, לכל קשת הגילאים והקשיים.  </p>
								<span>  צ׳יקונג  </span>
								<p class="p-t-10">  משנת 2000 מתרגל צ'י-קונג באופן עצמאי   </p>
								<p>  מעל 4 שנים מורה לצי' קונג - במרכז צ'י סנטר בישראל. כמו כן, מעביר קבוצות ושיעורים פרטיים מזה כ 4 שנים בקיבוץ שפיים.  </p>
								<span>  משנת 2000 ועד היום  </span>
								<p class="p-t-10">  ממשיך ומעמיק את הנסיון הקליני שלי בטיפולים בקליניקה , ונסיון ההוראה.<br>
ממשיך ללמוד בכל התחומים בקורסים והשתלמויות לאורך כל השנים    </p>
								
							</div>
						</div>						
					</div>
					<div style="height:6%;">						
								<div class="col-sm-12 text-center"> 
									<a href="<?php echo home_url(); ?>/צור קשר "><button type="submit" class="btn">  צור קשר  </button></a>
								</div>
							</form>
					</div>
				</div> <!--right-portion-->
			</div><!--row-->
		</div><!--respon-1-->
		
		
		<!--mobile view-->		
		<div class="mobile-about respon-2">
				<div style="height:70px; width:100%"></div>
				<div class="container-fluid">		
					<ul class="nav nav-pills text-center">
						<li class="bg-1" style="width:50%;"><a data-toggle="pill" href="#home" class="bgc-1"> הכשרות לימודים והסמכות </a></li>
						<li style="width:49.6%;" class="active bg-2"><a data-toggle="pill" href="#menu1" class="bgc-2"> יוחאי דור </a></li>
					</ul>
				</div>	<!--container-fluid-->  
					<div class="tab-content clearfix">
						<div id="home" class="tab-pane fade">
							<div class="container-fluid p-b-40">
								<div class="text-section">
									<div class="col-xs-12 "> <h3>הכשרות ולימודים מקצועיים בארץ ובחו״ל </h3> </div>
									<div style="width:55px; height:5px; background-color:#160925;  margin: 4px 15px 16px; float:right;" > </div>
									<div class="col-xs-12 p-b-20 first-por">
										<span> 1998 </span>
										<p> "רייקי מאסטר" 1-2-3 </p>
										<span> 2000 </span>
										<p> QiGong- Taoist inner alcomy-<br>Cosmic healing<br><br> עם מאסטר "מאנטאק צ'ייה".</p>
										<span> 2000-2004 </span>
										<p> טאו-שיאצו עם "ריוקיו אנדו סנסיי" , במרכז הישראלי לטאו-שיאצו.  </p>
										<span> 2007-2012 </span>
										<p> "סייקי שיאצו" עם צביקה קליסר סנסיי , בבית ספר לסייקי שיאצו ישראל.  </p>
										<span> 2014-2018 </span>
										<p>  Wisdom Healling Qigong (Zhineng) with master "Mington Gu" </p>
										<span> 2015-2017 </span>
										<p>  Sheng Zhen Gong with master Li Junfeng </p>
										<span> 2016-2018 </span>
										<p>  Zhineng Qigong with teachers from china Harmonious Big Family </p>
									</div><!--first-por-->
									
									<div class="col-xs-12 "> <h3>הסמכות </h3> </div>
									<div style="width:55px; height:5px; background-color:#160925; margin: 4px 15px 16px; float:right;" > </div>
									<div class="col-xs-12 p-b-10 second-por">
										<p>  מטפל בארץ ובחול בהילינג דרך צ'י-קונג </p>
										<p>  מטפל ב"רייקי"  </p>
										<p>  מטפל בכיר בסייקי שיאצו- טאו שיאצו  </p>
										<p>   מדריך ואסיסטנט בסייקי שיאצו לילדים ובוגרים </p>
										<p>   Wisdom Healling Qigong (Zhineng)- מורה בשיטת  </p>
										<p>   לפתיחת הלב - Sheng Zhen Gong - מורה בשיטת </p>
									</div><!--second-por-->
									
									<div class="col-xs-12 "> <h3> ניסיון </h3> </div>
									<div style="width:55px; height:5px; background-color:#160925; margin: 4px 15px 16px; float:right;" > </div>									
									<div class="col-xs-12 p-b-20 third-por">
										<span> רייקי  </span>
										<p>  טיפולים והדרכת קבוצות   </p>
										<span>  שיאצו  </span>
										<p class="p-t-10">  הדרכת והעברת קבוצות שיאצו לילדים, בגנים ובבתי ספר, ובניית מערכי שיעור במסגרות אלו  </p>
										<p>   מתן טיפולים בשיאצו, פרטניים , בילדים במגוון גילאים וקשיים </p>
										<p>   "פרוייקט אקי"ם" - מתן טיפולים שבועיים באקי"ים למשך כ-3 שנים </p>
										<p>  מדריך ואסיסטנט בבית ספר לשיאצו (בית ספר לסייקי שיאצו ישראל), מעל 3 שנים.  </p>
										<p>  נסיון של מעל 18 שנה במתן טיפולי שיאצו בקליניקה פרטית ובקופת חולים מכבי לכל קשת הגילאים ובמגוון רחב של קשיים ותחלואות (גוף -נפש)  </p>
										<span> הילינג   </span>
										<p class="p-t-10">  נסיון של מעל 18 שנה בטיפולים פרטניים בהילינג, לכל קשת הגילאים והקשיים.  </p>
										<span>  צ׳יקונג  </span>
										<p class="p-t-10">  משנת 2000 מתרגל צ'י-קונג באופן עצמאי   </p>
										<p> מעל 4 שנים מורה לצי' קונג - במרכז צ'י סנטר בישראל <br>כמו כן, מעביר קבוצות ושיעורים פרטיים מזה כ 4 שנים בקיבוץ שפיים.  </p>
										<span>  משנת 2000 ועד היום  </span>
										<p class="p-t-10">  ממשיך ומעמיק את הנסיון הקליני שלי בטיפולים בקליניקה , ונסיון ההוראה.<br>
		ממשיך ללמוד בכל התחומים בקורסים והשתלמויות לאורך כל השנים    </p>
										
									</div><!--third-por-->
								</div>
							</div>
							
				<a href="<?php echo home_url(); ?>/צור קשר" class="btn btn-block"> צור קשר </a>
						</div><!---tab-pane-1-->
						
						<div id="menu1" class="tab-pane fade in active">
							<div class="col-xs-12 mobile-banner-bottom ">
								<p>  שלום, שמי יוחאי דור, לומד מלמד, מנחה ומטפל מתמחה בתחום שיאצו, צ'י-קונג, והילינג .  </p>
							</div>
							<div class="clearfix"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-9.png" alt="image" style="width: 100%;" /></div>
							<div class="container-fluid p-b-40" style="padding:25px; text-align:right; color:#E5BC00;">
								<p>  ה"אני מאמין", כי אנו נמצאים בזמנים המבקשים מודעות והתעוררות לכח הריפוי הפנימי הנמצא בכל אדם.<br> אני מאמין באהבה ככוח העוצמתי ביותר לריפוי, כח המאפשר שינוי מהותי לחיים בריי קיימא.    </p>
								<div id="about-demo p-b-10">
									<p> תהליך הריפוי המתרחש תוך כדי טיפול, הינו חשיפה לטיבעינו המקורי, עידוד ופינוי מקום להזרמת אנרגיית החיים הנובעת מלב האדם פנימה.    </p>
									<p>  הטיפולים אינדיווידואלים ומותאמים באופן אישי, ההקשבה העמוקה לצרכים ולאדם הנמצא לפני,מאשרת לכל החסימות והדפוסים לצוף,לעלות ולהשתחרר. כך מתחזק הקשר עם העצמי האמיתי, והתוצאות מדהימות ומופלאות.   </p>
									<p>  שנים של התפתחות ועבודה עם רבים, והתגובות הנלהבות שאני מקבל ממטופלים ותלמידים, מוכיחות ומביאות אותי לידי הבנה בהירה, עד כמה חשובה הקריאה העכשווית לשימוש בכלים נגישים אלו, במתנות הטבע הברוכות. עד כמה משמעותית ומאפשרת שיפור ניכר באיכות החיים , בבריאות הפיזית הנפשית והרוחנית, ובמערכות יחסים עם עצמינו, האנשים סביבו ועם העולם הגדול שבחוץ.   </p>
									<p>    אני רוצה להודות, עם כל ליבי והערכתי, לכל המורים שליוו אותי לאורך כל השנים, <br>על הלימוד, ההשראה , הידע החכמה והבהירות שעברה דרכם אלי. </p>
									<p>   אני רוצה גם להודות למטופלים ולתמידים שפגשתי וליוויתי לאורך השנים והם היו <br>למורים הגדולים שלי! </p>
								</div><!--about-demo-->
							</div>
							<a href="<?php echo home_url(); ?>/צור קשר" class="btn btn-block"> צור קשר </a>		
						</div><!--tab-pane-2-->
					</div><!--tab-content-->
					
		</div><!-- mobile-view-->

	</div><!--blur-->		
</div>

<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

<!--blur-->
<script>   
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>


<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>

<!--background-color change-->
<script>
       $(document).ready(function(){
	   $(".bg-2").click(function(){
	  $(".mobile-about").css("background-color","#160925");
	  $(".nav-pills").css("background-color","#E5BC00");
	  $(".bgc-1").css("color","#160925");	 
	  });
	  $(".bg-1").click(function(){
	  $(".mobile-about").css("background-color","#E5BC00");
	  $(".nav-pills").css("background-color","#160925");
	  });
	  });
</script>

<!--main nav link color-->
<script>
       $(document).ready(function(){
	  $(".bg-1").click(function(){
	  $(".mb-c").css("color","#160925");
	  });
	  $(".bg-2").click(function(){
	  $(".mb-c").css("color","#E5BC00");  
	  });
	  });
</script>
<!--carsual-button-hide-show-->
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>
<!--swip Query in mobile on touch-->
	<script>
	(function ($) {
    var touchStartX = null;

    $('.carousel').each(function () {
        var $carousel = $(this);
        $(this).on('touchstart', function (event) {
            var e = event.originalEvent;
            if (e.touches.length == 1) {
                var touch = e.touches[0];
                touchStartX = touch.pageX;
            }
        }).on('touchmove', function (event) {
            var e = event.originalEvent;
            if (touchStartX != null) {
                var touchCurrentX = e.changedTouches[0].pageX;
                if ((touchCurrentX - touchStartX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('prev');
                } else if ((touchStartX - touchCurrentX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('next');
                }
            }
        }).on('touchend', function () {
            touchStartX = null;
        });
    });

})(jQuery);
	</script>


</body> <?php
get_footer();