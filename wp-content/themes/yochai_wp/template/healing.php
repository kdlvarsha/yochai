<?php
/**
 * Template Name: Healing Page 
 *
 * @package TheGem
 */
get_header(); ?>

<body class="light  healing" id="full-size">
<!-- Pre loader -->
<div id="loader" class="loader"></div>
<div id="app" class="">

<!--Sidebar End-->

	<div class="pos-f-t">
		<div class="collapse" id="navbarToggleExternalContent">
			<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
				<div class="search-bar">
					<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
						   placeholder="start typing...">
				</div>
				<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
				   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
			</div>
		</div>
	</div>
    <div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
            <!--Top Menu Start -->
		<ul class="nav navbar-nav">
				<!-- Notifications -->
			<li id="icon-show">
				<a class="nav-link ml-2" data-toggle="control-sidebar"><span> הילינג </span><i class="fa fa-bars" aria-hidden="true"></i></a>
			</li>
			<!-- User Account-->
		</ul>
    </div>
<div class="container-fluid blur">
		<div class="row" >
				<div class="col-md-12  respon-1" style="padding:0;">
					
					<div id="myCarousel" class="carousel" data-ride="carousel">
					  <!-- Indicators -->
					  <!-- <ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
						<li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
						<li data-target="#myCarousel" data-slide-to="5"></li>
					  </ol> -->

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner">
						<div class="carousel-item active">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="First Image">
						  
						</div>

						<div class="carousel-item">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="second Image">
						</div>

						<div class="carousel-item">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="third Image">
						</div>
						
						<div class="carousel-item">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="New York">
						</div>
						
						<div class="carousel-item">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="New York">
						</div>
						
						<div class="carousel-item">
						  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_desktop.png" style="width:100%" alt="New York">
						</div>
						
					  </div>
					</div>
							
				</div><!--respon-1-->
				
				
				<!--mobile version-->
				<div class="col-md-12  respon-2" style="padding:0;">				
					<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						
					  </ol>
						<div class="carousel-inner">
							<div class="carousel-item active">
							  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/healing_image_mobile.png" style="width:100%" alt="First Image">
							  
							</div>
						</div>
					</div>
					<div class="text bold text-3"><a href="tel:0523524381 " target="_blank"><span> להזמנת טיפול  </span><i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
				</div><!--respon-2-->
				
			<div class="col-sm-12  col-xs-12 " style="background: transparent;">
				<aside class="rightfix">
			 <h4 class="visible-xs-block" style="position:fixed; top:12%; right:25%;"> Source Energy Healing </h4>
						<div class="col-md-3 col-sm-12 col-xs-12 pull-right textbg mobile-scroll heal-scro-600">
							<div class="heal">
							 <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
							
							<div class="wprt-spacer" data-desktop="20" data-mobi="30" data-smobi="30" style="height:20px"></div>
							<div class="btn btnstyle hidden-xs" style="position: relative;"><a href="<?php echo home_url(); ?>/צור קשר "> להזמנת טיפול </a></div>
						</div>
					</div>
				</aside>
			</div>			
			
		</div><!--row-->
</div>

<!-- Right Sidebar -->
<aside class="control-sidebar fixed menubgcolor ">
    <div class="slimScroll">
        <div class="sidebar-header">
            <h5 class="pull-right webtext"> הילינג </h5>
            <a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
        </div>
		<div class="p-3 pull-right menustyle">
		<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
                	
		 <div class="btn btnstyle-1"><a href="<?php echo home_url(); ?>"> להזמנת טיפול </a></div>
		
			
		</div>
    </div>
</aside>
<!-- /.right-sidebar -->






<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
<div class="control-sidebar-bg shadow menubgcolor fixed"></div>




</div>
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<!--for blur--script on mobile version-->
<script>
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>



<!--full-screen slider auto every screen-->
<script>
var $item = $('.carousel-item'); 
var $wHeight = $(window).height();
$item.eq(0).addClass('active');
$item.height($wHeight); 
$item.addClass('full-screen');

$('.carousel img').each(function() {
  var $src = $(this).attr('src');
  var $color = $(this).attr('data-color');
  $(this).parent().css({
    'background-image' : 'url(' + $src + ')',
    'background-color' : $color
  });
  $(this).remove();
});

$(window).on('resize', function (){
  $wHeight = $(window).height();
  $item.height($wHeight);
});

$('.carousel').carousel({
  interval: 0,
  pause: "true"
});
</script>


<script>

       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<script>

       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });  
</script>

<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>

</body>