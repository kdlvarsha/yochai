<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

//get_header(); ?>
<div class="container">
<?php    
if(have_posts()) : while(have_posts()): the_post();
?>
      <div class="row">

        <!-- Post Content Column -->
        <div class="col-md-12">

          <!-- Title -->
          <h1 class="mt-4"><?php the_title(); ?></h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#"><?php echo get_the_author_meta('display_name', $author_id);  ?></a>
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on <?php echo get_the_date(); ?></p>

          <hr>

          <!-- Preview Image -->
         <div class="col-lg-6"> <?php 
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' ); 

if ($image) : ?>
<?php $ss=str_replace("-150x150","",$image[0]);?>
   <img src="<?php echo $ss; ?>" class="img-responsive" />
<?php endif; ?></div>
      
      <div class="col-lg-6 pull-right">
          <!-- Post Content -->
              <p class="lead"><p><?php the_content();?></p></p>
</div>

</div>

          

          <!-- Comments Form -->
          
       

      
      <!-- /.row -->
<?php endwhile; else: ?>
<p> no posts</p>
<?php endif; ?>
<?php if( have_rows('images') ): ?>
       
                  <?php while ( have_rows('images') ) : the_row(); ?>
                    <div class="scrollbar" id="style-4">
          <div class="force-overflow">
          <aside class="leftboximg">
            <div class="row">
                   <img class="img-responsive" src="<?php the_sub_field('image'); ?>" alt="image-1" />

</div>

</div></aside>
</div>
            

         
                    <?php endwhile; ?>
                <?php endif; ?>
    </div>
    <!-- /.container -->

  </div>

<hr>

         



<?php
get_footer();
