<?php
/**
 * Template Name:  Front Page
 *
 * @package TheGem
 */

get_header(); ?>

<div class="col-md-9 marpad0">
		
<div class="page height-full" id="background-slider">
			<!-- Slideshow container -->
			<div class="slideshow-container">
			  <!-- Full-width images with number and caption text -->
			  <div class="mySlides fad" style="display: block;">
				<img src="<?php echo get_bloginfo('template_directory'); ?>/img/demo.jpg" style="width:100%">
				<div class="text">שקט</div>
			  </div>

			  <div class="mySlides fad" style="display: none;">
				<img src="<?php echo get_bloginfo('template_directory'); ?>/img/demo.jpg" style="width:100%">
				<div class="text">שקט</div>
			  </div>

			  <div class="mySlides fad" style="display: none;">
				<img src="<?php echo get_bloginfo('template_directory'); ?>/img/demo.jpg" style="width:100%">
				<div class="text">שקט</div>
			  </div>

			  <!-- Next and previous buttons -->
			  <div style="text-align:center" class="circle-bottom">
			  <span class="dot active" onclick="currentSlide(1)"></span> 
			  <span class="dot" onclick="currentSlide(2)"></span> 
			  <span class="dot" onclick="currentSlide(3)"></span>
			  <span class="dot" onclick="currentSlide(4)"></span> 
			  <span class="dot" onclick="currentSlide(5)"></span> 
			  <span class="dot" onclick="currentSlide(6)"></span>			  
			</div>
			</div>
			<!-- The dots/circles -->		
</div>

	</div>
	<div class="col-md-3">
	<aside class="rightfix">
			<div class="col-md-3 col-sm-12 col-xs-12 pull-right textbg">
				<p>שיטת טיפול וריפוי בעזרת מגע ולחיצות עמוקות, ממוקדות ומדויקות מאד. </p>
				<p>הטיפול מתבצע בלבוש מלא, מותאם אישית למטופל,  משפיע על  המרידיאניים - ערוצי האנרגיה בגוף. כך נפתחות חסימות, ומתרחשת זרימה המאפשרת נשימה פנימה, שחרור וחיבורעם יכולת הריפוי הטבעית שלנו. </p>
				<p>הריפוי מתרחש בו זמנית בכל המישורים, ומביא אותנו להרגשה טובה, נעימה, בטוחה ושלמה. </p>
				<p>לאורך זמן, ניתן לטפל במגוון בעיות/ מצוקות, החל מכאבי ראש, צוואר, גב, עייפות כרונית, בעיות עיכול, נשימה עור, בעיות נפשיות ועוד.
במהלך הטיפול, נכנס הגוף למצב רגיעה מוחלטת, המגע במרכזי עצבים בגוף, גורם לשחרור נוירוטרנסמיטרים מסוג סרוטונין, המאפשר וגורם לשחרור מלחצים, דאגות וטרדות היום יום. במצב זה יכולת הריפוי של הגוף את עצמו מתאפשרת במיטבה ועולה לאין שיעור. </p>
<p>לאחר הטיפול מתקבל תחושה של קלילות, חיבור, שיחרור, איזון, אושר, איכות חיים ובריאות קורנת. </p>
               <div class="btn btnstyle"><a href="<?php echo home_url(); ?>/"> טקסט דמה </a></div> </div>
    </aside>
			</div>	

<aside class="control-sidebar fixed menubgcolor ">
   <?php get_sidebar(); ?>
</aside>
<div class="control-sidebar-bg shadow menubgcolor fixed"></div>


</div>
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>


<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>


</body>
