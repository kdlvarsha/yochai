<?php
/**
 * Template Name: Main Page
 *
 * @package TheGem
 */
get_header(); ?>


</div>
<div class="page height-full">
	<div class="bgimages">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="fullspace">
                        <div class="circlefirst">
                            <div class="firsbtnround"><a href="<?php echo home_url(); ?>/front/">צ׳יקונג </a></div>						
                            <div class="first-circle "></div>
                            <div class="firstborder "></div>
                        </div>
                         <div class="circlesec">
                            <div class="firsbtnround-2"><a href="<?php echo home_url(); ?>/front/">הילינג </a></div>
                            <div class="second-circle"></div>
                            <div class="firstborder-2"></div>
                        </div>
						<div class="circlethird">
                                <div class="firsbtnround-3"><a href="<?php echo home_url(); ?>/front/">שיאצו </a></div>
                                <div class="third-circle"></div>
                                <div class="firstborder-3"></div>
                                <div class="roundtext">
                                    <h3>Source Energy Healing</h3> 
                                    <p> טקסט דמה </p>
                         		</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Right Sidebar -->
<aside class="control-sidebar fixed menubgcolor " style="width:144px!important;">
    <div class="slimScroll">
        <div class="sidebar-header">
            <h4 class="pull-right webtext"> סייקי שיאצו </h4>
            <a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
        </div>
		<div class="p-3 pull-right menustyle">
			<ul>
				<li> <a href="<?php echo home_url(); ?>/front/"> דף הבית </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> שיאצו </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> הילינג </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> צ׳יקונג </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> אודות </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> השראה </a></li>
				<li> <a href="<?php echo home_url(); ?>/front/"> צור קשר </a></li>
			</ul>
		</div>
    </div>
</aside>
<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:144px!important;"></div>
</div>
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>

<script>
$(document).ready(function() {     
    $('.circlefirst a').hover(function(){  
	     $('.circlefirst').addClass('circle-border'); 
        $('.first-circle').addClass('ico-circle');    
		$('.firstborder').addClass('border-horizontal');   
		   
    },     
    function(){   
		$('.circlefirst').removeClass('circle-border'); 
        $('.first-circle').removeClass('ico-circle'); 
			$('.firstborder').removeClass('border-horizontal');   
		
    });
});   
</script>

<script>
$(document).ready(function() {     
    $('.circlesec a').hover(function(){  
	     $('.circlesec').addClass('circle-border'); 
        $('.second-circle').addClass('ico-circle');    
		$('.firstborder-2').addClass('border-horizontal');   
		   
    },     
    function(){   
		$('.circlesec').removeClass('circle-border'); 
        $('.second-circle').removeClass('ico-circle'); 
			$('.firstborder-2').removeClass('border-horizontal');   
		
    });
});   
</script>

<script>
$(document).ready(function() {     
    $('.circlethird a').hover(function(){  
	     $('.circlethird').addClass('circle-border'); 
        $('.third-circle').addClass('ico-circle');    
		$('.firstborder-3').addClass('border-horizontal');   
		   
    },     
    function(){   
		$('.circlethird').removeClass('circle-border'); 
        $('.third-circle').removeClass('ico-circle'); 
			$('.firstborder-3').removeClass('border-horizontal');   
		
    });
});   
</script>

</body>
