<?php
/**
 * Template Name: Inspiration
 *
 * @package TheGem
 */
get_header(); ?>

<body class="inspiration cong-menu" id="full-size">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> השראה  </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> השראה   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
					<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		
	<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid inspiration-yochai respon-1">
			<div class="row">
			    <!--left-portion-->
				<div class="col-sm-9 center-img">
				    <div class="mobile-scroll round" style="height:100%; overflow-y:scroll; margin-right:10px;">
						<div class="text-section">
							<?php
        // the query
        $the_query = new WP_Query(array(
            'category_name' => 'השראה',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ));
        ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<div class="col-sm-4 p-20"> 
							
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5>   <?php the_title(); ?></h5> </div>
									 <?php the_post_thumbnail(); ?>
										<div class="caption">
											<?php the_excerpt(); ?>
											<div class="collapse" id="demo-1"> <?php the_content(); ?>
											</div>
											<div style="padding-top:15px;"><a href="#" data-toggle="collapse" data-target="#demo-1">  < קרא עוד  </a>
												</div>
										</div>
									</div>
								</div>
								
							</div><!--col-sm-4-->  <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>

        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
							
							
						</div>	<!---text-section-->					
					</div><!--mobile-scroll-->
				</div><!--sm-9-->
				
				<!--right-portion-->
				<div class="col-sm-3 inspiration-right-text" style="padding-right: 0; height:100%">
					<div style="height:20%; width:100%"></div>
					<div class="mobile-scroll" style="height:80%; overflow-y:scroll; ">
						<div class="text-section">
							<div class="col-sm-12 p-b-20 "> <?php dynamic_sidebar('inspiration'); ?> </div>
							<p style="padding-right:20px;color:#19132a;"> Albert Einstein </p>
						</div>						
					</div>
				</div> <!--right-portion-->
			</div><!--row-->
		</div><!--respon-1-->
		
		
		<!--mobile view-->		
		<div class="respon-2 mobile-inspiration clearfix">
			<div style="height:70px; width:100%; background-color:#E5BC00;"></div>
			<div class="mobile-insp-scroll" style="height:30vh; overflow-y:scroll; margin-right:12px; margin-bottom: 20px">
				<div class="text-section text-center;">
					<div class="col-xs-12 p-b-20 text-center"> <span>  “we cannot solve our problems with the same thinking we created them”  </span> </div>
					<p style="color:#19132a; text-align: center;"> Albert Einstein </p>
				</div>	
			</div>
			
			<div class="col-xs-12 second-portion">				
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-10.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן.<br>
												הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח…  </p>
											<div class="collapse" id="demo-9"><p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן.<br>
												הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח…  </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-9">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-11.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו....  </p>
											<div class="collapse" id="demo-10"><p>   </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-10">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-12.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו.... </p>
											<div class="collapse" id="demo-11"><p>   </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-11">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-13.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן.<br>
												הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח  הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן. הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן. הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח…
											</p>
											<div class="collapse" id="demo-12"><p>  </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-12">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-14.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן.<br>הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח…  </p>
											<div class="collapse" id="demo-13"><p>   </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-13">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-15.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו.... </p>
											<div class="collapse" id="demo-14"><p>    </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-14">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5> הפילוסופיה של הסייקי שיאצו  </h5> </div>
										<img src="<?php echo get_bloginfo('template_directory'); ?>/img/Capture-16.png" alt="img"/>
										<div class="caption">
											<p>  הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן.<br>הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח  הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן. הסייקי שיאצו התפתח מיסודות רפואת המזרח והוא מכיל את הבסיס לגישות הטיפוליות העתיקות הסיניות וגישות הריפוי שהתפתחו מאוחר יותר ביפן. הסייקי שיאצו כשיטת טיפול נותן מקום למסורות הריפוי העתיקות שמקורן במזרח…  </p>
											<div class="collapse" id="demo-15"><p>   </p>
											</div>
											<span><a href="#" data-toggle="collapse" data-target="#demo-15">  < קרא עוד  </a></span>
										</div>
									</div>
								</div>
				
			</div><!--col-xs-12-->
		</div><!-- mobile-view-->
		
		
	</div><!--blur-->			
</div>

<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

<!--blur-->
<script>   
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>

<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<!--carsual-button-hide-show-->
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>  <?php wp_footer(); ?>