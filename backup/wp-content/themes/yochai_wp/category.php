<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

<body class="inspiration cong-menu" id="full-size">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> השראה  </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> השראה   </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
					<ul>
					<li> <a href="<?php echo home_url(); ?>"> דף הבית </a></li>
                	<li> <a href="<?php echo home_url(); ?>/שיאצו/">שיאצו</a></li>
                	<li> <a href="<?php echo home_url(); ?>/הילינג""> הילינג </a></li>
                	<li> <a href="<?php echo home_url(); ?>/ צ׳יקונג "> צ׳יקונג </a></li>
                	<li> <a href="<?php echo home_url(); ?>/ אודות "> אודות </a></li>
                	<li> <a href="<?php echo home_url(); ?>/ השראה "> השראה </a></li>
                	<li> <a href="<?php echo home_url(); ?>/צור קשר""> צור קשר </a></li>
                	 
					</ul>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		
	<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid inspiration-yochai respon-1">
			<div class="row">
			    <!--left-portion-->
				<div class="col-sm-9 center-img">
				    <div class="mobile-scroll round" style="height:100%; overflow-y:scroll; margin-right:10px;">
					
						<div class="text-section">
						 
  
						
						<?php $i=1; if ( have_posts() ) : while ( have_posts() ) :the_post();  ?>	
					
					  
					    <div class="col-sm-4 p-20"> 
							
								<div class="col-xs-12 p-0">
								   	<div class="thumbnail">
								   	    	<div class="caption">  <h5><?php echo the_title(); ?> </h5></div>
						<?php if ( has_post_thumbnail() ) : ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(); ?>
    </a>
<?php endif; ?>
<?php echo the_content(); ?>

												</div>
												
												
										</div>
									</div>
								
							<?php $i++;	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>

							
						
								
						</div>
						</div>
						</div>
								
							
						
								
								
						
				
				<!--right-portion-->
				<div class="col-sm-3 inspiration-right-text" style="padding-right: 0; height:100%">
					<div style="height:15%; width:100%"></div>
					<div class="mobile-scroll" style="height:85%; overflow-y:scroll; ">
						<div class="text-section" style="text-align: center;">
							<div class="col-sm-12 p-b-20 "> <span>  “we cannot solve our problems with the same thinking we created them”  </span> </div>
							<p style="float:right; padding-right:20px;color:#19132a;"> Albert Einstein </p>
						</div>						
					</div>
				</div> <!--right-portion-->
			</div><!--row-->
		</div><!--respon-1-->
		
		
		<!--mobile view-->		
		<div class="respon-2 mobile-inspiration clearfix">
			<div style="height:70px; width:100%; background-color:#E5BC00;"></div>
			<div class="mobile-insp-scroll" style="height:30vh; overflow-y:scroll; margin-right:12px; margin-bottom: 20px">
				<div class="text-section text-center;">
					<div class="col-xs-12 p-b-20 text-center"> <span>  “we cannot solve our problems with the same thinking we created them”  </span> </div>
					<p style="color:#19132a; text-align: center;"> Albert Einstein </p>
				</div>	
			</div>
			
			<div class="col-xs-12 second-portion">
			    <?php $i=1; if ( have_posts() ) : while ( have_posts() ) :the_post();  ?>					
								<div class="col-xs-12 p-0">
									<div class="thumbnail">
										<div class="caption">  <h5><?php echo the_title(); ?></h5>  </h5> </div>
										<?php if ( has_post_thumbnail() ) : ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(); ?>
    </a>
<?php endif; ?>
<?php echo the_content(); ?>
										
											
										</div>
									</div>
		

<?php $i++;	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div>
	
<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<!-- or -->
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

<!--blur-->

<script>   
       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>


<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<!--carsual-button-hide-show-->
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
	  
</script>

<?php

get_footer(); ?>