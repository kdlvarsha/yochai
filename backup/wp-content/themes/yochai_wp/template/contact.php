<?php
/**
 * Template Name: Contact
 *
 * @package TheGem
 */
get_header(); ?>


<body class=" contact-us-menu" id="full-size" class="">
<!-- Pre loader -->

 
<div id="loader" class="loader"></div>
<div id="app" >
		
<!--Sidebar End-->
		<div class="">
			<div class="pos-f-t">
				<div class="collapse" id="navbarToggleExternalContent">
					<div class="bg-dark pt-2 pb-2 pl-4 pr-2">
						<div class="search-bar">
							<input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
								   placeholder="start typing...">
						</div>
						<a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
						   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
					</div>
				</div>
			</div>
			<div class="navbar navbar-expand navbar-dark pull-right" id="navbar">
					<!--Top Menu Start -->
				<ul class="nav navbar-nav">
						<!-- Notifications -->
					<li id="icon-show"> <a class="nav-link ml-2" data-toggle="control-sidebar"><span> צור קשר  </span><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
					<!-- User Account-->
				</ul>
			</div>

		</div>
		
		<!-- Right Sidebar -->
		<aside class="control-sidebar fixed menubgcolor " style="width:px!important;">
			<div class="slimScroll">
				<div class="sidebar-header">
					<h5 class="pull-right webtext"> צור קשר  </h5>
					<a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
				</div>
				<div class="p-3 pull-right menustyle">
						<?php wp_nav_menu(array('menu'=>'header_menu','container'=>'','menu_id' =>''));?>
				</div>
			</div>
		</aside>
		<div class="control-sidebar-bg shadow menubgcolor fixed" style="width:px!important;"></div>
		<div class="blur">
		<!-- contact-body-->
		<div class="container-fluid contact-yochai respon-1">
			<div class="row">
			    <!--left-contact-->
				<div class="col-sm-4 left-contact contact-scroll">
					<div class="contact-event">

<?php
        // the query
        $the_query = new WP_Query(array(
            'category_name' => 'שם האירוע ',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ));
        ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            	<div  class="col-sm-12 font-right-b" style="font-size:21px; font-weight:normal;">שיעורים / סדנאות
            	</div>
						
							<div class="col-sm-7 font-left-n">
							<div style="display:inline-block;"><?php the_time('l, Y/m/d') ?></div>
						</div>	
								
						<div class="col-sm-5 font-right-b"><?php the_title(); ?></div>
											
									
						<div class="col-sm-10 col-sm-offset-2 font-right-b p-b-10"><?php the_excerpt(); ?></div>	

						<div class="col-sm-12 font-left-n"><a href="#" class="rectangle-4-copy">לפרטים נוספים</a>  </div>			
						<!--col-sm-4-->  <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>

        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>


						
						
					</div>
				</div>
				
				<!--middle-contact-->
				<div class="col-sm-5 center-contact text-right">
				    <?php dynamic_sidebar('Contact Middle'); ?>
				</div>
				
				<!--right-contact-->
				<div class="col-sm-3 right-contact " style="padding-right: 0; border-bottom: 1px solid #fff;">
					<div style="height:70px; width:100%"></div>
					<div class="mobile-scroll con-scro-600">
						<?php dynamic_sidebar('Contact Sidebar'); ?></div>
				</div>
			</div>
		</div>
		
		
		<!--mobile view-->
		
		
		<div class="moble-contact respon-2" style="background-color:#160925;" >
			<div style="text-align:center" class="portion-1">
				<div class="col-xs-12" style="background-color:#E5BC00; border-radius:50px; padding:1px 2px 1px 1px;">
					<div class="col-xs-4" style="padding-left:0;">
					  <span class="dot" onclick="currentSlide(1)">  שירותים  </span> 
					</div>
					<div class="col-xs-4">
					  <span class="dot" onclick="currentSlide(2)">  לפרטים  </span>
					</div>
					<div class="col-xs-4" style="padding-right:0;">
					  <span class="dot" onclick="currentSlide(3)">  שלח/י הודעה  </span> 
					</div>
				</div>
			</div>

			<div class="slideshow-container portion-2">
				 
					<div class="mySlides fde">
							<div style="position:relative; height:70%;">									 
									  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/contact_image_mobile.png" style="width:100%">
									 <!-- <div class="text"><a href="#">  התקשר/י עכשיו     <i class="fa fa-phone"></i></a></div>-->
									 <div class="text bold text-3"><a href="tel:0523524381 " target="_blank">  התקשר/י עכשיו     <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
							</div>
							<div style="position:relative; height:30%;">
								<div class="first-contact" style="text-align:right;">
									<div  class="col-sm-12 font-right-b p-t-20"><a href="#">שירותים</a></div>
									<div class="col-md-12 p-b-20"><span class="font-right-b">   טיפולים - </span>
						    <a href="http://source-energy.co.il/הילינג"><span class="font-left-n">הילינג </span></a><a href="<?php echo home_url(); ?>/שיאצו"><span class="font-left-n">  |  סייקי שיאצו</span></a></div>
									<div class="col-md-12 p-b-40"><div class="font-left-n" style="display:inline-block;" >     פרטי וקבוצות  <a href="<?php echo home_url(); ?>/ צ׳יקונג "> |  (zhineng) צ'י קונג </a> -  </div><div class="font-right-b" style="display:inline-block">שיעורים    </div></a></div>
								</div>
							</div>
					</div>

					<div class="mySlides fde">
							<div style="position:relative; height:70%;">									 
									  <img src="<?php echo get_bloginfo('template_directory'); ?>/img/contact_image_mobile.png" style="width:100%">
									  <div class="text bold text-3"><a href="tel:0523524381 " target="_blank">  התקשר/י עכשיו     <i><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone-bottun-icon.png" style="margin-right: -6px; margin-top: -4px; margin-bottom: -5px;"></i> </a></div>
							</div>
							<div style="position:relative; height:30%;">
								<div class="contact-section">
									<div class="col-sm-12 p-b-20 bold" style="font-weight:bold;">יוחאי דור</div>
									<div class="col-sm-12 p-b-10 ">052-3524381<span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/phone_icon.png"/></div>
									<div class="col-sm-12 p-b-10 "> קיבוץ שפיים  <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/></div>
									<div class="col-sm-12 p-b-20 "> רחוב הברזל 47 תל אביב <span class="p-l-10"><img src="<?php echo get_bloginfo('template_directory'); ?>/img/location-icon.png"/> </div>
								</div>
							</div>					
					</div>

					<div class="mySlides fde">
					<div class="main-form">
					    <div class="contact-form">
							
							<?php echo do_shortcode('[contact-form-7 id="144" title="Untitled"]'); ?>
						</div>
					</div>
					</div>
			
					<!--<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
						<a class="next" onclick="plusSlides(1)">&#10095;</a>
				  -->
				
			</div>
		</div><!-- mobile-view-->
		</div>		
		
            </div>
            </div>
    </div>
    </div>
    


<!--/#app -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/app.js"></script>
<script>

       $(document).ready(function(){ 
  	   
	  $("#icon-show").click(function(){
	  if (window.matchMedia('(max-width: 767px)').matches) {
        //...
	  $(".blur").css("filter","blur(2px)");	  
    }
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".blur").css("filter","blur(0)"); 
	  });	 
	  });
</script>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>
<script>

       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $("#icon-show").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $("#icon-show").show();  
	  });
	  });
</script>
<script>
       $(document).ready(function(){
	  $("#icon-show").click(function(){
	  $(".text-3").hide();
	  });
	  $(".paper-nav-toggle").click(function(){
	  $(".text-3").show();  
	  });
	  });
</script>

</body>